from utils import dist


class Deck:
    def __init__(self, cards):
        self.cards = cards
        self.i = -1
        self.next()

    def next(self):
        self.i += 1
        if self.i == len(self.cards):
            self.i = 0

    def prev(self):
        self.i -= 1
        if self.i == -1:
            self.i = len(self.cards) - 1

    def get_top(self):
        return self.cards[self.i]

    def set_top(self, card):
        self.cards[self.i] = card


class Team:
    def __init__(self, pitchers, lineup, bench, field=None):
        self.pitchers = pitchers
        self.lineup = lineup
        self.bench = bench
        self.field = field


class Inning:
    def __init__(self, pitchers, lineup, bench, field):
        self.pitchers = pitchers
        self.lineup = lineup
        self.bench = bench
        self.field = field

    def swap_hitter(self):
        out = self.lineup.get_top()
        ph = self.bench.get_top()
        self.lineup.set_top(ph)
        self.bench.set_top(out)


class Game:
    def __init__(self, stadium, visitor, home):
        self.stadium = stadium
        self.visitor = visitor
        self.home = home

        self.top = Inning(home.pitchers, visitor.lineup, visitor.bench, home.field)
        self.bot = Inning(visitor.pitchers, home.lineup, home.bench, visitor.field)


class ClickPoint:
    def __init__(self, pos, radius, on=False, drag=False):
        self.pos = pos
        self.radius = radius
        self.on = on
        self.drag = False
        self.br = 0

    def contains(self, point):
        return dist(self.pos, point) < self.radius

    def next_br(self):
        self.br = (self.br + 1) % 6

    def prev_br(self):
        self.br = (self.br - 1) % 6
