import os
import sys

import pygame
from PIL import Image, ImageDraw, ImageFont

from utils import FONT_PATH

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)

CHART_DEFENSE = {
    "LF": ("+1 / -2", "-1"),
    "CF": ("+2 / -1", "+1"),
    "RF": ("+1 / +2", "-1"),
    "1B": ("4 / 2", ""),
    "2B": ("2 / 3", ""),
    "P": ("1 / 4", ""),
    "SS": ("3 / 5", ""),
    "3B": ("5 / 6", ""),
}


def get_chart_image():
    image = print_defense(CHART_DEFENSE, chart_key=True)
    return convert_for_game(image)


def get_baseball_field_image(defense):
    image = print_defense(defense)
    return convert_for_game(image)


def convert_for_game(image):
    img = pygame.image.fromstring(image.tobytes(), image.size, image.mode)
    scale = 0.62
    w, h = int(img.get_width() * scale), int(img.get_height() * scale)
    img = pygame.transform.smoothscale(img, (w, h))
    return img.convert()


def print_defense(defense, chart_key=False):
    # Check if MEIPASS attribute is available in sys else return current file path
    bundle_dir = getattr(sys, "_MEIPASS", os.path.abspath(os.path.dirname(__file__)))
    img_path = os.path.abspath(os.path.join(bundle_dir, "images/baseball_field.png"))
    img = Image.open(img_path)
    draw = ImageDraw.Draw(img)
    font_size = 38 if chart_key else 32
    font = ImageFont.truetype(FONT_PATH, font_size)

    if chart_key:
        line1 = "S: 2→H / 1→3"
        line2 = "D: 1→H"
        w1, h = font.getsize(line1)
        x = (img.width - w1) / 10.0
        draw.text((x, 650), line1, WHITE, font)
        w = font.getsize(line2)[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 650 + h + 4), line2, WHITE, font)

    if "LF" in defense:
        w1, h = font.getsize(defense["LF"][0])
        x = (img.width - w1) / 8.0
        draw.text((x, 140), defense["LF"][0], WHITE, font)
        w = font.getsize(defense["LF"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 140 + h + 4), defense["LF"][1], WHITE, font)

    if "CF" in defense:
        w1, h = font.getsize(defense["CF"][0])
        x = (img.width - w1) / 2.0
        draw.text((x, 40), defense["CF"][0], WHITE, font)
        w = font.getsize(defense["CF"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 40 + h + 4), defense["CF"][1], WHITE, font)

    if "RF" in defense:
        w1, h = font.getsize(defense["RF"][0])
        x = 7 * (img.width - w1) / 8.0
        draw.text((x, 140), defense["RF"][0], WHITE, font)
        w = font.getsize(defense["RF"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 140 + h + 4), defense["RF"][1], WHITE, font)

    if "3B" in defense:
        w1, h = font.getsize(defense["3B"][0])
        x = (img.width - w1) / 4.0
        draw.text((x, 400), defense["3B"][0], BLACK, font)
        w = font.getsize(defense["3B"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 400 + h + 4), defense["3B"][1], BLACK, font)

    if "1B" in defense:
        w1, h = font.getsize(defense["1B"][0])
        x = 3 * (img.width - w1) / 4.0
        draw.text((x, 400), defense["1B"][0], BLACK, font)
        w = font.getsize(defense["1B"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 400 + h + 4), defense["1B"][1], BLACK, font)

    if "SS" in defense:
        w1, h = font.getsize(defense["SS"][0])
        x = (img.width - w1) / 3.0
        draw.text((x, 300), defense["SS"][0], BLACK, font)
        w = font.getsize(defense["SS"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 300 + h + 4), defense["SS"][1], BLACK, font)

    if "2B" in defense:
        w1, h = font.getsize(defense["2B"][0])
        x = 2 * (img.width - w1) / 3.0
        draw.text((x, 300), defense["2B"][0], BLACK, font)
        w = font.getsize(defense["2B"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 300 + h + 4), defense["2B"][1], BLACK, font)

    if "P" in defense:
        w1, h = font.getsize(defense["P"][0])
        x = (img.width - w1) / 2.0
        draw.text((x, 445), defense["P"][0], WHITE, font)

    if "C" in defense:
        w1, h = font.getsize(defense["C"][0])
        x = (img.width - w1) / 2.0
        draw.text((x, 730), defense["C"][0], WHITE, font)
        w = font.getsize(defense["C"][1])[0]
        x = x + (w1 - w) / 2.0
        draw.text((x, 730 + h + 4), defense["C"][1], WHITE, font)

    return img


def parse_lineup(filename):
    with open(filename, "r") as f:
        positions = set()
        defense = {}
        for line in f:
            if line.strip() and line[0] != "#":
                pos, name, drating = [x.strip() for x in line.split(",")]
                positions.add(pos)
                defense[pos] = (name.split(" ")[-1], drating)
        defense["file"] = filename
        return defense
