# IP Player

A simple set of tools for playing Inside Pitch with the PDF cards.

## Pregame

Run the `game.exe` file.

When the program starts, it will initialize a cards folder with the following contents:

```
cards/
  away/
    defense.csv
    batters/
      bench/
    pitchers/
  home/
    defense.csv
    batters/
      bench/
    pitchers/
```

It's your job to put your cards in the right places:

A stadium card should be placed in the cards folder alongside the `away` and `home` directories.

The starting lineup batter cards should be placed in the `batters` folder.
The reserve batter cards should be placed in the `batters/bench` folder.
All pitchers should be placed in the `pitchers` folder.
Each deck of cards will be sorted in alphabetic order, so name your starting lineup using numerical prefixes i.e. `01_`, `02_`, etc.

You should also edit the `defense.csv` files for both teams with entries for each player:

```
1B,BREWER,5 (2)
#2B,IGNORE,1 (20) -1
SS,RIPKEN,5 (6) +1
```

If you'd like to hide one of the entries in the `defense.csv` file, you can prefix a line with a pound sign (`#`).

## Game Controls

### Working with the card decks

- Move through a deck = near the top of a card, use the scroll wheel OR left click/right click to flip cards
- Switch innings = spacebar
- Show bench = `b`
- Swap bench player with the top of the lineup deck = `s`

### Manipulating runners

- Show baserunner advancement chart = `c`
- Create a runner = Left click on the field to create a runner
- Move a runner = Left click and drag
- Change BR rating of a runner = Right click on the runner OR scroll
- Delete a runner = Middle click the runner

### Changing the defense

To change the defense, edit the `defense.csv` file and save...press d to make the changes visible.
If there is an error parsing the file, the load will not happen.
