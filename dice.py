import math
import os
import random
import sys

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1"

import pygame
import pygame.freetype
import pygame.gfxdraw


def aa_round_rect(surface, rect, color, rad=20, border=0, inside=(0, 0, 0)):
    rect = pygame.Rect(rect)
    _aa_render_region(surface, rect, color, rad)
    if border:
        rect.inflate_ip(-2 * border, -2 * border)
        _aa_render_region(surface, rect, inside, rad)


def _aa_render_region(image, rect, color, rad):
    corners = rect.inflate(-2 * rad - 1, -2 * rad - 1)
    for attribute in ("topleft", "topright", "bottomleft", "bottomright"):
        x, y = getattr(corners, attribute)
        pygame.gfxdraw.aacircle(image, x, y, rad, color)
        pygame.gfxdraw.filled_circle(image, x, y, rad, color)
    image.fill(color, rect.inflate(-2 * rad, 0))
    image.fill(color, rect.inflate(0, -2 * rad))


def drawRegularPolygon(surface, color, numSides, tiltAngle, x, y, radius):
    pts = []
    for i in range(numSides):
        x = x + radius * math.cos(tiltAngle + math.pi * 2 * i / numSides)
        y = y + radius * math.sin(tiltAngle + math.pi * 2 * i / numSides)
        pts.append([int(x), int(y)])
    pygame.gfxdraw.aapolygon(surface, pts, color)
    pygame.gfxdraw.filled_polygon(surface, pts, color)


RED = (200, 20, 20)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (11, 102, 35)
GRAY = (192, 192, 192)
SCREEN_SIZE = WIDTH, HEIGHT = 450, 150
# Check if MEIPASS attribute is available in sys else return current file path
bundle_dir = getattr(sys, "_MEIPASS", os.path.abspath(os.path.dirname(__file__)))
FONT_PATH = os.path.abspath(os.path.join(bundle_dir, "images/Gotham_Bold.otf"))


class D6:
    def __init__(self, size, die_color, spots_color):
        self.die_color = die_color
        self.spots_color = spots_color
        self.size = size
        self.roller = random.SystemRandom()

    def draw(self, on):
        n = self.roller.randrange(1, 7)
        side = self.size
        s = pygame.Surface((side, side), pygame.SRCALPHA, 32)
        s.fill(GRAY)
        aa_round_rect(
            s, (0, 0, side, side), self.die_color, rad=8, inside=self.die_color
        )
        radius = side // 10
        if on and (n == 1 or n == 3 or n == 5):
            self.circle(s, (side // 2, side // 2), radius)  # middle
        if on and (n == 2 or n == 3 or n == 5 or n == 4 or n == 6):
            self.circle(s, (side // 5, 4 * side // 5), radius)  # bleft
            self.circle(s, (4 * side // 5, side // 5), radius)  # tright
        if on and (n == 4 or n == 5 or n == 6):
            self.circle(s, (side // 5, side // 5), radius)  # tleft
            self.circle(s, (4 * side // 5, 4 * side // 5), radius)  # bright
        if on and (n == 6):
            self.circle(s, (side // 5, side // 2), radius)  # tleft
            self.circle(s, (4 * side // 5, side // 2), radius)  # bright
        return s

    def circle(self, s, pt, r):
        x, y = pt
        pygame.gfxdraw.aacircle(s, x, y, r, self.spots_color)
        pygame.gfxdraw.filled_circle(s, x, y, r, self.spots_color)


class D20:
    def __init__(self, size, die_color, num_color):
        self.die_color = die_color
        self.num_color = num_color
        self.size = size
        self.roller = random.SystemRandom()

    def draw(self, on):
        n = self.roller.randrange(1, 21)
        s = pygame.Surface((self.size * 2, self.size * 2), pygame.SRCALPHA, 32)
        s.fill(GRAY)
        drawRegularPolygon(s, self.die_color, 6, math.pi / 6, self.size, -1, self.size)
        x, r = font.render(str(n))
        if on:
            font.render_to(
                s, (self.size - r.w // 2, self.size - r.h // 2), str(n), self.num_color
            )
        return s


if __name__ == "__main__":
    pygame.display.init()
    pygame.font.init()
    pygame.display.set_caption("Dice", "Dice")
    pygame.freetype.init()
    font = pygame.freetype.Font(FONT_PATH, 20)
    scr = pygame.display.set_mode(SCREEN_SIZE)

    red_d6 = D6(50, RED, WHITE)
    white_d6 = D6(50, WHITE, BLACK)
    blue_d20 = D20(30, BLUE, WHITE)
    green_d6 = D6(50, GREEN, WHITE)

    blue_on, green_on, red_on = False, False, False
    scr.fill(GRAY)
    red_d6_rect = scr.blit(red_d6.draw(red_on), (50, 50))
    white_d6_rect = scr.blit(white_d6.draw(red_on), (150, 50))
    blue_d20_rect = scr.blit(blue_d20.draw(blue_on), (245, 46))
    green_d6_rect = scr.blit(green_d6.draw(green_on), (350, 50))

    pygame.display.flip()
    while True:
        event = pygame.event.wait()

        if event.type == pygame.QUIT:
            raise SystemExit
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                scr.fill(GRAY)
                if red_d6_rect.collidepoint(event.pos) or white_d6_rect.collidepoint(
                    event.pos
                ):
                    red_on = not red_on
                    green_on, blue_on = True, True
                    for i in range(20):
                        pygame.time.wait(10)
                        red_d6_rect = scr.blit(red_d6.draw(red_on), (50, 50))
                        white_d6_rect = scr.blit(white_d6.draw(red_on), (150, 50))
                        pygame.display.flip()
                elif blue_d20_rect.collidepoint(event.pos):
                    blue_on = not blue_on
                    green_on, red_on = True, True
                    for i in range(20):
                        pygame.time.wait(10)
                        blue_d20_rect = scr.blit(blue_d20.draw(blue_on), (245, 46))
                        pygame.display.flip()
                elif green_d6_rect.collidepoint(event.pos):
                    green_on = not green_on
                    blue_on, red_on = True, True
                    for i in range(20):
                        pygame.time.wait(10)
                        green_d6_rect = scr.blit(green_d6.draw(green_on), (350, 50))
                        pygame.display.flip()
                else:
                    red_on, blue_on, green_on = False, False, False
                    red_d6_rect = scr.blit(red_d6.draw(red_on), (50, 50))
                    white_d6_rect = scr.blit(white_d6.draw(red_on), (150, 50))
                    blue_d20_rect = scr.blit(blue_d20.draw(blue_on), (245, 46))
                    green_d6_rect = scr.blit(green_d6.draw(green_on), (350, 50))

                pygame.display.flip()
