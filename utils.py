import math
import os
import sys

import pygame

SCALE = 0.275
# Check if MEIPASS attribute is available in sys else return current file path
bundle_dir = getattr(sys, "_MEIPASS", os.path.abspath(os.path.dirname(__file__)))
FONT_PATH = os.path.abspath(os.path.join(bundle_dir, "images/ARIALNB.TTF"))


def dist(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    dist = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return dist


def build_cards_from_dir(path):
    filepaths = []
    for file in os.listdir(path):
        filepath = os.path.join(path, file)
        if os.path.isfile(filepath):
            filepaths.append(filepath)
    cards = [Card(p) for p in sorted(filepaths)]
    if cards:
        return cards
    else:
        return [Card()]


def s(x):
    return int(SCALE * x)


def scale(surface):
    w, h = surface.get_rect().size
    return pygame.transform.smoothscale(surface, (s(w), s(h)))


class Card:
    def __init__(self, path=None):
        if path and os.path.isfile(path):
            surface = pygame.image.load(path)
            surface = scale(surface)
            surface = surface.convert()
            self.surface = surface
        else:
            self.surface = scale(
                pygame.Surface(
                    (
                        1654,
                        2116,
                    )
                )
            )
