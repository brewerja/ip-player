import os
import sys
from pathlib import Path

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1"

import pygame
import pygame.gfxdraw

from defense import get_baseball_field_image, get_chart_image, parse_lineup
from models import ClickPoint, Deck, Game, Team
from utils import FONT_PATH, Card, build_cards_from_dir

SCREEN_SIZE = WIDTH, HEIGHT = 460 * 4 + 28, 581 + 10
BG_COLOR = (0, 159, 13)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED_ALPHA = (250, 0, 0, 60)
RED = (255, 0, 0)
RADIUS = int(0.62 * 20)


def s(x):
    return int(0.62 * x)


OUT1 = (1385 + s(40), 5 + s(900))
OUT2 = (1385 + s(105), 5 + s(900))
OUTS = [OUT1, OUT2]


class FieldDisplay:
    def __init__(self, field, pos):
        self.field = field
        self.pos = pos
        self.images = [get_baseball_field_image(field), CHART_IMAGE]
        w, h = self.images[0].get_size()
        self.rect = pygame.Rect(pos[0], pos[1], w, h)
        self.drag_rect = pygame.Rect(
            self.rect.x + RADIUS,
            self.rect.y + RADIUS,
            self.rect.w - 2 * RADIUS,
            self.rect.h - 2 * RADIUS,
        )
        self.i = 0
        self.float_points = []
        self.click_points = [ClickPoint(x, radius=RADIUS + 5) for x in OUTS]

    def blit(self):
        SCREEN.blit(self.images[self.i], self.pos)
        self.blit_click_points()
        self.blit_float_points()

    def toggle(self):
        self.i = (self.i + 1) % len(self.images)
        self.blit()

    def reload_defense_image(self):
        try:
            field = parse_lineup(self.field["file"])
            self.field = field
            self.images[0] = get_baseball_field_image(field)
            self.blit()
            return True
        except:
            print("Failed to parse " + self.field["file"])
            return False

    def click(self, event):
        redraw = False
        if event.button == 1:
            for point in self.click_points:
                if point.contains(event.pos):
                    point.on = not point.on
                    redraw = True
            if not redraw:
                new_pt = True
                for point in self.float_points:
                    if point.contains(event.pos):
                        point.drag = True
                        new_pt = False
                if new_pt:
                    self.float_points.append(ClickPoint(event.pos, RADIUS, on=True))
                redraw = True
        elif event.button == 2:
            self.float_points = [
                p for p in self.float_points if not p.contains(event.pos)
            ]
            redraw = True
        elif event.button in (3, 4):
            for point in self.float_points:
                if point.contains(event.pos):
                    point.next_br()
                    redraw = True
        elif event.button == 5:
            for point in self.float_points:
                if point.contains(event.pos):
                    point.prev_br()
                    redraw = True

        if redraw:
            self.blit()
            return True

    def mouse_button(self, event):
        redraw = False
        if event.type == pygame.MOUSEBUTTONUP:
            for point in self.float_points:
                if point.drag:
                    point.pos = event.pos
                    point.drag = False
                    redraw = True
        elif event.type == pygame.MOUSEMOTION:
            for point in self.float_points:
                if point.drag and self.drag_rect.collidepoint(event.pos):
                    point.pos = event.pos
                    redraw = True
        if redraw:
            self.blit()
            return True

    def blit_click_points(self):
        sur = FONT.render("OUTS", True, WHITE)
        r = sur.get_rect()
        outs_txt_pos = ((OUT1[0] + OUT2[0]) // 2 - r.w // 2, s(835))
        SCREEN.blit(sur, outs_txt_pos)
        for point in self.click_points:
            x, y = point.pos
            pygame.gfxdraw.aacircle(SCREEN, x, y, RADIUS + 5, WHITE)
            if point.on:
                pygame.gfxdraw.aacircle(SCREEN, x, y, RADIUS, WHITE)
                pygame.gfxdraw.filled_circle(SCREEN, x, y, RADIUS, WHITE)

    def blit_float_points(self):
        for point in self.float_points:
            if point.on:
                x, y = point.pos
                pygame.gfxdraw.aacircle(SCREEN, x, y, RADIUS, RED)
                pygame.gfxdraw.filled_circle(SCREEN, x, y, RADIUS, RED)
                if point.br:
                    sur = FONT.render(str(point.br), True, WHITE)
                    r = sur.get_rect()
                    txt_pos = (x - r.w // 2, y - r.h // 2)
                    SCREEN.blit(sur, txt_pos)


class DeckDisplay:
    def __init__(self, deck, pos, on=True):
        self.deck = deck
        self.pos = pos
        self.on = on
        top_card = self.deck.get_top().surface
        self.rect = pygame.Rect((pos[0], pos[1]), top_card.get_size())

    def blit(self):
        top_card = self.deck.get_top().surface
        self.rect = pygame.Rect(
            self.pos[0], self.pos[1], top_card.get_width(), top_card.get_height()
        )
        self.flip_rect = pygame.Rect(
            self.pos[0],
            self.pos[1],
            top_card.get_width(),
            int(top_card.get_height() / 6),
        )
        SCREEN.blit(top_card, self.pos)


class InningDisplay:
    def __init__(self, inning, stadium):
        y = 5
        self.inning = inning
        self.pitchers = DeckDisplay(inning.pitchers, (5, y))
        self.lineup = DeckDisplay(inning.lineup, (465, y))
        self.stadium = DeckDisplay(stadium, (925, y))
        self.bench = DeckDisplay(inning.bench, (1385, y), on=False)
        self.field = FieldDisplay(inning.field, (1385, y))

    def blit(self):
        SCREEN.fill(BG_COLOR)
        self.pitchers.blit()
        self.lineup.blit()
        self.blit_batting_number()
        self.stadium.blit()
        if self.bench.on:
            self.bench.blit()
        else:
            self.field.blit()

    def blit_batting_number(self):
        s = FONT.render(str(self.lineup.deck.i + 1), True, BLACK)
        r = s.get_rect()
        SCREEN.blit(s, (465 + 8 + int(r.w / 2), 5 + 28))

    def toggle_bench(self):
        self.bench.on = not self.bench.on
        if self.bench.on:
            SCREEN.fill(BG_COLOR, pygame.Rect((1385, 5), self.field.rect.size))
            self.bench.blit()
        else:
            SCREEN.fill(BG_COLOR, pygame.Rect((1385, 5), self.bench.rect.size))
            self.field.blit()

    def swap_hitter(self):
        self.inning.swap_hitter()
        self.blit()

    def click(self, event):
        for display in [self.pitchers, self.lineup, self.bench]:
            if display.on and display.flip_rect.collidepoint(event.pos):
                if event.button in (1, 5):
                    display.deck.next()
                    self.blit()
                    return True
                elif event.button in (3, 4):
                    display.deck.prev()
                    self.blit()
                    return True
        if event.button == 1:
            if self.draw_highlight(*event.pos):
                return True
        elif event.button == 2:
            if self.erase_highlight(*event.pos):
                return True

        if not self.bench.on and self.field.rect.collidepoint(event.pos):
            return self.field.click(event)

    def draw_highlight(self, x, y):
        for display in [self.pitchers, self.lineup, self.stadium]:
            if display.rect.collidepoint((x, y)):
                w, h = 55, 30
                s = pygame.Surface((w, h), pygame.SRCALPHA)
                pygame.draw.rect(s, RED_ALPHA, (0, 0, w, h), 0)
                SCREEN.blit(s, (x - int(w / 2), y - int(h / 2)))
                return True

    def erase_highlight(self, x, y):
        w, h = 180, 90
        for display in [self.pitchers, self.lineup, self.stadium]:
            if display.rect.collidepoint((x, y)):
                SCREEN.blit(
                    display.deck.get_top().surface,
                    (x - int(w / 2), y - int(h / 2)),
                    (
                        x - int(w / 2) - display.rect.x,
                        y - int(h / 2) - display.rect.y,
                        w,
                        h,
                    ),
                )
                return True


class GameDisplay:
    def __init__(self, game):
        self.game = game
        self.top = InningDisplay(game.top, game.stadium)
        self.bot = InningDisplay(game.bot, game.stadium)
        self.inning = self.top

    def toggle_inning(self):
        self.inning.field.float_points = []
        for point in self.inning.field.click_points:
            point.on = False
        self.inning = self.bot if self.inning == self.top else self.top
        self.blit()

    def blit(self):
        self.inning.blit()


def build_game(visitor_path, home_path):
    stadium = Deck([Card()])
    path = "cards"
    for file in os.listdir(path):
        filepath = os.path.join(path, file)
        if os.path.isfile(filepath):
            stadium = Deck([Card(filepath)])

    pitchers = Deck(build_cards_from_dir(visitor_path + "pitchers"))
    lineup = Deck(build_cards_from_dir(visitor_path + "batters"))
    bench = Deck(build_cards_from_dir(visitor_path + "batters/bench"))
    visitor_defense = parse_lineup(visitor_path + "defense.csv")
    visitor = Team(pitchers, lineup, bench, visitor_defense)

    pitchers = Deck(build_cards_from_dir(home_path + "pitchers"))
    lineup = Deck(build_cards_from_dir(home_path + "batters"))
    bench = Deck(build_cards_from_dir(home_path + "batters/bench"))
    home_defense = parse_lineup(home_path + "defense.csv")
    home = Team(pitchers, lineup, bench, home_defense)

    return Game(stadium, visitor, home)


def main():
    global SCREEN
    global FONT
    global CHART_IMAGE

    visitor_path = "cards/away/"
    os.makedirs(visitor_path + "batters/bench", exist_ok=True)
    os.makedirs(visitor_path + "pitchers", exist_ok=True)
    visitor_defense = Path(visitor_path + "defense.csv")
    if not os.path.isfile(visitor_defense):
        visitor_defense.touch()

    home_path = "cards/home/"
    os.makedirs(home_path + "batters/bench", exist_ok=True)
    os.makedirs(home_path + "pitchers", exist_ok=True)
    home_defense = Path(home_path + "defense.csv")
    if not os.path.isfile(home_defense):
        home_defense.touch()

    pygame.display.init()
    pygame.font.init()
    pygame.display.set_caption("Inside Pitch", "IP")
    # Check if MEIPASS attribute is available in sys else return current file path
    bundle_dir = getattr(sys, "_MEIPASS", os.path.abspath(os.path.dirname(__file__)))
    icon_path = os.path.abspath(os.path.join(bundle_dir, "images/icon.jpg"))
    icon = pygame.transform.smoothscale(pygame.image.load(icon_path), (64, 64))
    pygame.display.set_icon(icon)
    SCREEN = pygame.display.set_mode(SCREEN_SIZE)
    SCREEN.fill(BG_COLOR)
    FONT = pygame.font.Font(FONT_PATH, 24)
    CHART_IMAGE = get_chart_image()

    pygame.display.flip()

    game = build_game(visitor_path, home_path)
    game_display = GameDisplay(game)

    game_display.blit()
    pygame.display.flip()

    while True:
        event = pygame.event.wait()

        if event.type == pygame.QUIT:
            raise SystemExit
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if game_display.inning.click(event):
                pygame.display.flip()
        elif event.type == pygame.MOUSEMOTION or event.type == pygame.MOUSEBUTTONUP:
            if game_display.inning.field.mouse_button(event):
                pygame.display.update(game_display.inning.bench.rect)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                game_display.toggle_inning()
                pygame.display.flip()
            elif event.key == pygame.K_b:
                game_display.inning.toggle_bench()
                pygame.display.flip()
            elif event.key == pygame.K_c and not game_display.inning.bench.on:
                game_display.inning.field.toggle()
                pygame.display.flip()
            elif event.key == pygame.K_s and game_display.inning.bench.on:
                game_display.inning.swap_hitter()
                pygame.display.flip()
            elif event.key == pygame.K_d and not game_display.inning.bench.on:
                if game_display.inning.field.reload_defense_image():
                    pygame.display.flip()


if __name__ == "__main__":
    main()
