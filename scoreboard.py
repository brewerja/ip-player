import csv
import os
import sys

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1"

import pygame
import pygame.freetype

WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
# Check if MEIPASS attribute is available in sys else return current file path
bundle_dir = getattr(sys, "_MEIPASS", os.path.abspath(os.path.dirname(__file__)))
FONT_PATH = os.path.abspath(os.path.join(bundle_dir, "images/Gotham_Bold.otf"))
STORAGE_FILE = "scoreboard.csv"


class Scoreboard:
    def __init__(self):
        self.refresh()

    def increase_cell(self, row, col):
        v = self.rows[row][col + 1]
        self.rows[row][col + 1] = str(int(v) + 1) if v else "0"
        self.write()

    def decrease_cell(self, row, col):
        if self.rows[row][col + 1]:
            v = int(self.rows[row][col + 1])
            self.rows[row][col + 1] = str(v - 1) if v else ""
            self.write()

    def clear_cell(self, row, col):
        if col + 1 < len(self.rows[row]) - 4:
            self.rows[row][col + 1] = ""
        else:
            self.rows[row][col + 1] = "0"
        self.write()

    def update_run_totals(self):
        for row in self.rows[1:]:
            row[-4] = str(sum([int(x) for x in row[1:-4] if x]))

    def write(self):
        self.update_run_totals()
        with open(STORAGE_FILE, "w") as csvfile:
            w = csv.writer(csvfile, lineterminator="\n")
            for row in self.rows:
                w.writerow(row)

    def refresh(self):
        self.rows = []
        with open(STORAGE_FILE) as f:
            for line in f:
                self.rows.append(line.strip().split(","))

    def get_num_innings(self):
        return len(self.rows[0]) - 1


sb = Scoreboard()


if __name__ == "__main__":
    pygame.display.init()
    pygame.font.init()
    pygame.freetype.init()
    pygame.display.set_caption("Scoreboard", "SC")
    font = pygame.freetype.Font(FONT_PATH, 25)
    sm_font = pygame.freetype.Font(FONT_PATH, 15)

    first = True
    RHE = 150
    num_innings = sb.get_num_innings()
    screen_size = width, height = (50 + num_innings * 50, 90)
    screen = pygame.display.set_mode(screen_size)
    while True:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            raise SystemExit
        elif (
            not first
            and event.type != pygame.MOUSEBUTTONDOWN
            and event.type != pygame.ACTIVEEVENT
        ):
            continue
        first = False

        pad = 5

        r1_y = 2
        r2_y = pad + 25
        r3_y = 2 * (pad + 25)
        y = [r1_y, r2_y, r3_y]

        x0 = 125
        cell_w = 50

        if sb.get_num_innings() != num_innings:
            num_innings = sb.get_num_innings()
            screen_size = width, height = (50 + num_innings * 50, 90)
            screen = pygame.display.set_mode(screen_size)
        sb.refresh()

        screen.fill(BLACK)

        visitor_name = sb.rows[1][0].upper()
        home_name = sb.rows[2][0].upper()
        name_w = max(font.render(visitor_name)[1].w, font.render(home_name)[1].w)

        s, red_bar_txt = sm_font.render("LOB")
        pygame.draw.rect(screen, RED, (0, 0, width, red_bar_txt.h + 4))
        font.render_to(screen, (pad, r2_y), visitor_name, WHITE)
        font.render_to(screen, (pad, r3_y), home_name, WHITE)

        for i, row in enumerate(sb.rows):
            x = x0
            for j, cell in enumerate(row[1 : len(row) - 4]):
                f = font if i != 0 else sm_font
                x += cell_w
                s, rect = f.render(cell)
                xx = x + (cell_w - rect.w) // 2
                if i != 0 and event.type == pygame.MOUSEBUTTONDOWN:
                    click_x, click_y = event.pos
                    if (
                        click_x > x
                        and click_x < x + cell_w
                        and click_y > y[i]
                        and click_y < y[i] + pad + 25
                    ):
                        if event.button == 1:
                            sb.increase_cell(i, j)
                        elif event.button == 2:
                            sb.clear_cell(i, j)
                        elif event.button == 3:
                            sb.decrease_cell(i, j)
                        cell = sb.rows[i][j + 1]
                        s, rect = f.render(cell)
                        xx = x + (cell_w - rect.w) // 2
                f.render_to(screen, (xx, y[i]), cell, WHITE)

        pygame.draw.rect(screen, RED, (x + cell_w, 0, 15, height))

        x0 = x + cell_w // 2
        for i, row in enumerate(sb.rows):
            x = x0
            for j, cell in enumerate(row[len(row) - 4 :]):
                f = font if i != 0 else sm_font
                color = WHITE if i == 0 or j != 0 else YELLOW
                x += cell_w
                s, rect = f.render(cell)
                xx = x + (cell_w - rect.w) // 2
                if i != 0 and event.type == pygame.MOUSEBUTTONDOWN:
                    click_x, click_y = event.pos
                    if (
                        j != 0
                        and click_x > x
                        and click_x < x + cell_w
                        and click_y > y[i]
                        and click_y < y[i] + pad + 25
                    ):
                        if event.button == 1:
                            sb.increase_cell(i, j + len(row) - 5)
                        elif event.button == 2:
                            sb.clear_cell(i, j + len(row) - 5)
                        elif event.button == 3:
                            sb.decrease_cell(i, j + len(row) - 5)
                        cell = sb.rows[i][j + len(row) - 4]
                        s, rect = f.render(cell)
                        xx = x + (cell_w - rect.w) // 2
                f.render_to(screen, (xx, y[i]), cell, color)

        pygame.display.flip()
